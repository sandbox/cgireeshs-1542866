<?php
/**
 * Administrator interface for JQUERY Quiz module.
 *
 * @file
 */

// JQUERY QUIZ ADMIN

// JQUERY Quiz Admin Settings

/**
 * This builds the main settings form for the jquery quiz module.
 */
function jqquiz_admin_settings() {
  $form = array();
  $form['jqquiz_look_feel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Quiz Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Leave it for future additions of settings'),
  );
/*
  $form['jqquiz_look_feel']['jqquiz_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Display name'),
    '#default_value' => JQUERY_QUIZ_NAME,
    '#description' => t('Change the name of the quiz type. Do you call it <em>test</em> or <em>assessment</em> instead? Change the display name of the module to something else. Currently, it is called @quiz. By default, it is called <em>JQuery Quiz</em>.',
      array('@quiz' => JQUERY_QUIZ_NAME)),
    '#required' => TRUE,
  );
*/
  $form['jqquiz_email_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Settings'),
    '#description' => t('Leave it for future additions of settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
/*
  $form['jqquiz_email_settings']['taker'] = array(
    '#type' => 'fieldset',
    '#title' => t('E-mail for Quiz Takers'),
    '#collapsible' => FALSE,
  );

  $form['jqquiz_email_settings']['taker']['quiz_email_results'] = array(
    '#type' => 'checkbox',
    '#title' => t('E-mail results to quiz takers'),
    '#default_value' => variable_get('quiz_email_results', 0),
    '#description' => t('Check this to send users their results at the end of a quiz.')
  );

  $form['jqquiz_email_settings']['taker']['quiz_email_results_subject_taker'] = array(
    '#type' => 'textfield',
    '#title' => t('Configure E-mail Subject'),
    '#description' => t('This format will be used when sending quiz results at the end of a quiz.'),
    '#default_value' => variable_get('quiz_email_results_subject_taker', jqquiz_email_results_format('subject', 'taker')),
  );

  $form['jqquiz_email_settings']['taker']['quiz_email_results_body_taker'] = array(
    '#type' => 'textarea',
    '#title' => t('Configure E-mail Format'),
    '#description' => t('This format will be used when sending quiz results at the end of a quiz. !title(quiz title), !sitename, !taker(quiz takers username), !date(time when quiz was finished), !minutes(How many minutes the quiz taker spent taking the quiz), !desc(description of the quiz), !correct(points attained), !total(max score for the quiz), !percentage(percentage score), !url(url to the result page) and !author are placeholders.'),
    '#default_value' => variable_get('quiz_email_results_body_taker', jqquiz_email_results_format('body', 'taker')),
  );

  $form['jqquiz_email_settings']['author'] = array(
    '#type' => 'fieldset',
    '#title' => t('E-mail for Quiz Authors'),
    '#collapsible' => FALSE,
  );

  $form['jqquiz_email_settings']['author']['quiz_results_to_quiz_author'] = array(
    '#type' => 'checkbox',
    '#title' => t('E-mail all results to quiz author.'),
    '#default_value' => variable_get('quiz_results_to_quiz_author', 0),
    '#description' => t('Check this to send quiz results for all users to the quiz author.'),
  );

  $form['jqquiz_email_settings']['author']['quiz_email_results_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Configure E-mail Subject'),
    '#description' => t('This format will be used when sending quiz results at the end of a quiz. Authors and quiz takers gets the same format.'),
    '#default_value' => variable_get('quiz_email_results_subject', jqquiz_email_results_format('subject', 'author')),
  );

  $form['jqquiz_email_settings']['author']['quiz_email_results_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Configure E-mail Format'),
    '#description' => t('This format will be used when sending quiz results at the end of a quiz. !title(quiz title), !sitename, !taker(quiz takers username), !date(time when quiz was finished), !minutes(How many minutes the quiz taker spent taking the quiz), !desc(description of the quiz), !correct(points attained), !total(max score for the quiz), !percentage(percentage score), !url(url to the result page) and !author are placeholders.'),
    '#default_value' => variable_get('quiz_email_results_body', jqquiz_email_results_format('body', 'author')),
  );
  $form['#validate'][] = 'quiz_settings_form_validate';
  $form['#submit'][] = 'quiz_settings_form_submit';


  	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#submit' => array(),
	); 
  */

 // return system_settings_form($form);
  return $form;
  
  
}

/**
  * Submit the admin settings form
  */
function jqquiz_settings_form_submit($form, &$form_state) {
  if (JQUERY_QUIZ_NAME != $form_state['values']['jqquiz_name']) {
    variable_set('jqquiz_name', $form_state['values']['jqquiz_name']);
    define(JQUERY_QUIZ_NAME, $form_state['values']['jqquiz_name']);
    menu_rebuild();
  }
}