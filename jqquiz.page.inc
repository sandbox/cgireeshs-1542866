<?php
// $Id: ajax_load_example.page.inc,v 1.1 2008/12/17 20:01:54 nedjo Exp $

/**
 * @file
 * Menu callbacks and associated methods for Ajax load example.
 */

/**
 * Menu callback: present a page with a link.
 */
function jqquiz_page() {
  $path = drupal_get_path('module', 'jqquiz');
  
  drupal_add_css(drupal_get_path('module', 'certificate') . '/css/dialog.css');
  
  
  $quiz_cat =  l(theme_image(drupal_get_path('module', 'jqquiz').'/images/category.jpeg'),'jqquiz/callback', array('attributes' => array('class' =>'ajax-load-example','id'=>'category','title'=>'Question Category'),'html' => true));
  $quiz_question_mgmnt =  l(theme_image(drupal_get_path('module', 'jqquiz').'/images/qmgmnt.jpeg'),'jqquiz/callback', array('attributes' => array('class' =>'ajax-load-example','id'=>'question','title'=>'Manage Question'),'html' => true));
  $quiz_edit = l(theme_image(drupal_get_path('module', 'jqquiz').'/images/edit.jpeg'),'jqquiz/callback', array('attributes' => array('class' =>'ajax-load-example','id'=>'QuizList','title'=>'Quiz List'),'html' => true)); 
 
 
 $out='';
  $out.='<div width="100%" id="qouter"><div style="border:2px solid #e4e4e4;margin-top:0px;"><div align="right" style="margin-top:0px;padding-top:0px;padding-right:5px;padding-bottom:0px;"></div><div style="background:url('.$path.'/images/bgimg.jpeg);repeat-x;height:40px;padding-top:0px;" width="100%">';
  $out.='<div style="float:left;padding:5px 10px 2px 5px;">'.$quiz_cat.'</div>';
	$out.='<div style="float:left;padding:5px 10px 2px 5px;">'.$quiz_question_mgmnt.'</div>';
	$out.='<div style="float:left;padding:5px 10px 2px 5px;">'.$quiz_edit.'</div>';
  

  
	$out.='</div><br><div align="center"><div id="loader" style="display:none;"  width="100%"><img src="'.$path.'/images/loading.gif"/></div><div align="left" id="disptitle" style="display:none;"><span id="sectionhead"></span></div><div id="qcontent" style="text-align:left;overflow: auto;" width="100%"></div></div>';
  $out.='<div style="height:80px;border-top:0px solid #999999;width:100%;"><div></div></div></div>';


  
 
  // If this is an AJAX request, return the AJAX result.
  if (isset($_REQUEST['jqquiz']) && $_REQUEST['jqquiz']) {
    jqquiz_callback();
  }else if(isset($_REQUEST['deletecategory']) && $_REQUEST['deletecategory']){
    jqquiz_callbackdeletecategory();
  }
  else if(isset($_REQUEST['category']) && $_REQUEST['category']){
    jqquiz_callbackcategory();
  }else if(isset($_REQUEST['addquiz']) && $_REQUEST['addquiz']){
   jqquiz_callback();
  
  }else if(isset($_REQUEST['editquiz']) && $_REQUEST['editquiz']){
   jqquiz_callback_edit();
  
  }else if(isset($_REQUEST['AddCategory']) && $_REQUEST['AddCategory']){
    
  jqquiz_callbackaddcategory();
  }else if(isset($_REQUEST['EditCategory']) && $_REQUEST['EditCategory']){
   
 // $tid = $_REQUEST['tid'];
  
  jqquiz_callbackeditcategory();
  }else if(isset($_REQUEST['QuizList']) && $_REQUEST['QuizList']){
  
  jqquiz_callback_quizlist();
  
  }else if(isset($_REQUEST['reports']) && $_REQUEST['reports']){
    
 // jqquiz_report_callback();
  reports_gui_callback();
  }else if(isset($_REQUEST['attempt_details']) && $_REQUEST['attempt_details']){
    
 // jqquiz_report_callback();
  reports_details_callback();
  }else if(isset($_REQUEST['question_details']) && $_REQUEST['question_details']){
    
 // jqquiz_report_callback();
  reports_question_userdetails_callback();
  }
  
  
  
  else if(isset($_REQUEST['deletequestion']) && $_REQUEST['deletequestion']){
  
  jqquiz_callbackdelete_question();
  
  }
  
  else if(isset($_REQUEST['question']) && $_REQUEST['question']){
  
  jqquiz_managequestioncallback();
  
  }else if(isset($_REQUEST['QuizList']) && $_REQUEST['QuizList']){
  
  jqquiz_callback_quizlist();
  
  }else if(isset($_REQUEST['addquestion']) && $_REQUEST['addquestion']){ //add question callback
  
  jqquiz_addquestioncallback();
  
  }else if(isset($_REQUEST['editquestion']) && $_REQUEST['editquestion']){ //edit question callback
  
  jqquiz_editquestioncallback();
  
  }else if(isset($_REQUEST['quiz_question_options']) && $_REQUEST['quiz_question_options']){   //dropdown question options callback function
  
  jqquiz_question_optioncallback();
  
  }else if(isset($_REQUEST['change_quiz_status']) && $_REQUEST['change_quiz_status']){   //change status of quiz,questions,certificate callback function
  
  jqquiz_change_quiz_statuscallback();
  
  }else if(isset($_REQUEST['change_category_status']) && $_REQUEST['change_category_status']){   //change status of quiz,questions,certificate callback function
  
  jqquiz_change_category_statuscallback();
  
  }
    else if(isset($_REQUEST['change_question_status']) && $_REQUEST['change_question_status']){   //change status of quiz,questions,certificate callback function
  
  jqquiz_change_question_statuscallback();
  
  }else if(isset($_REQUEST['courses']) && $_REQUEST['courses']){   //callback function For loading course modules
  
  jqquiz_coursemodule_callback();
  
  }else {
    drupal_add_js(drupal_get_path('module', 'jqquiz') . '/js/jqquiz.js');
   // drupal_add_js(drupal_get_path('module', 'jqquiz') . '/ajax_load_example.js');

   // return '<div>' . l(t('Load test form via AJAX'), 'jqquiz/callback', array('attributes' => array('class' => 'ajax-load-example'))) . '</div>';
   return $out; 
  }
    // If this is an AJAX request, return the AJAX result.

}

/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_callback() {
  $result = array(
    'content' => drupal_get_form('jqquiz_form'),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_form');
  drupal_json($result);
}


/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_callback_edit() {
  $result = array(
    'content' => drupal_get_form('jqquiz_edit_form'),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_edit_form');
  drupal_json($result);
}







/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_callback2() {
  $result = array(
    'content' => drupal_get_form('jqquiz_questiontypes'),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_questiontypes');
  drupal_json($result);
}


/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_callbackcategory() {
  $result = array(
    'content' => jqquiz_get_category(),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_get_category');
  drupal_json($result);
}



/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_callbackdeletecategory() {
  $result = array(
    'content' => jqquiz_get_category(),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_get_category');
  drupal_json($result);
}



/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_callbackaddcategory() {

$node = new stdClass();
//$node->type = 'custom';


  $result = array(
    'content' => drupal_get_form('jqquiz_category_form',$node),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_category_form');
  drupal_json($result);
}


/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */

function jqquiz_callbackeditcategory() {
$node = new stdClass();
//$node->type = 'custom';


  $result = array(
    'content' => drupal_get_form('jqquiz_category_edit_form',$node),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('category_ajax_data', $result, 'jqquiz', 'jqquiz_category_edit_form');
  drupal_json($result);
}


/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_callbackquestion() {
$node = new stdClass();
//$node->type = 'custom';


  $result = array(
    'content' => drupal_get_form('certificate_ajax_upload_form',$node),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'certificate_ajax_upload_form');
  drupal_json($result);
}

/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */


function jqquiz_callback_quizlist() {
$result = array(
    'content' => jqquiz_list_quiz(),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_list_quiz');
  drupal_json($result);
}



/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */

function jqquiz_managequestioncallback() {
$result = array(
    'content' => jqquiz_list_question(),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_list_question');
  drupal_json($result);
}



/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */

function jqquiz_addquestioncallback() {
$result = array(
    'content' => drupal_get_form('jqquiz_question_form',$node),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_question_form');
  drupal_json($result);
}


/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_editquestioncallback() {
$result = array(
    'content' => drupal_get_form('jqquiz_question_edit_form',$node),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_question_edit_form');
  drupal_json($result);
}




/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_callbackdelete_question() {
  $result = array(
    'content' => jqquiz_list_question(),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_list_question');
  drupal_json($result);
}



/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */
function jqquiz_copyquestioncallback() {
  $result = array(
    'content' => jqquiz_copy_question(),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_copy_question');
  drupal_json($result);
}



/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */

function jqquiz_change_quiz_statuscallback(){

  $result = array(
    'content' => jqquiz_change_quiz_status(),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_change_quiz_status');
  drupal_json($result);


}





/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */

function jqquiz_change_category_statuscallback(){

  $result = array(
    'content' => jqquiz_change_category_status(),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_change_category_status');
  drupal_json($result);


}





/**
 * Render a form in JSON.
 *
 * This function shows how to generate and alter an AJAX response to allow
 * ajax_load to add its functionality.
 */

function jqquiz_change_question_statuscallback(){
  $result = array(
    'content' => jqquiz_change_question_status(),
    //Put the Javascript callback you will use here.
    //You can if you wish leave out this line and instead
    //call your callback directly in your Javascript. See 
    //comments in ajax_load_example.js.
    '__callbacks' => array('Drupal.jqquizExample.formCallback'),
  );
  //Call drupal_alter('ajax_data', ...). This is what allows ajax_load to
  //add its data and register its Javascript callback.
  //The second argument is the data to be altered.
  //The third argument is a unique identifier for this AJAX data operation.
  //The fourth and optional argument is the original data that was the subject
  //of the ajax operation--in this case, a form ID.
  drupal_alter('ajax_data', $result, 'jqquiz', 'jqquiz_change_question_status');
  drupal_json($result);
}

