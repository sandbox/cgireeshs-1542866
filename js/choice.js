/**
* Force a minimum width of a country field so that inline state fields
* don't jump around on the page.
*/
Drupal.behaviors.questions = function(context) {
  
  $('#quiz_id', context).bind('change',function(){
     $("#loader").show();
          $("#disptitle").hide();	
   var target = $("#qcontent");
  var cat = "question";
            $.ajax({
            // Either GET or POST will work.
            type: 'POST',
            data: cat+'=1&quiz_id='+this.value,
            // Need to specify JSON data.
            dataType: 'json',
            url: $(this).attr('href'),
            success: function(response){
		    $("#loader").hide();
		     $("#disptitle").show();
		   //  sectionhead
		      var tit = $("#sectionhead").html("<b>" + ucfirst(cat)+ "</b>");
		    $("#sectionhead").text(tit);

              // Call all callbacks.
              if (response.__callbacks) {
                $.each(response.__callbacks, function(i, callback) { 
                  // The first argument is a target element, the second
                  // the returned JSON data.
                  eval(callback)(target, response);
                });
                // If you don't want to return this module's own callback,
                // you could of course just call it directly here.
                 Drupal.jqquizExample.formCallback(target, response);
              }
            }
          });
  
  
  });
  

};