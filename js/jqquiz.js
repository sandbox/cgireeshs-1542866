// $Id: jqquiz.js,v 1.0 2012/04/10 14:01:06  Exp $

(function ($) {

Drupal.jqquizExample = Drupal.jqquizExample || {};

/**
 * Ajax load example behavior. 
 */
Drupal.behaviors.jqquizExample = function (context) {
  $('a.ajax-load-example:not(.ajax-load-example-processed)', context)
    .each(function () {
      // The target should not be e.g. a node that will itself be
      // replaced, as this would mean no node is available for
      // ajax_load to attach behaviors to when all scripts are loaded.
    //  var target = this.parentNode.innerHTML;
	    
	 var target = $("#qcontent");
	    
      $(this)
        .addClass('ajax-load-example-processed')
        .click(function () {
	var cat = this.id;
	
	  if(cat=='deletecategory'){
	  
	  if(confirm("Do you want to delete this category")){
	  
	  var cat = this.id;
	  }else{
	  
	  return false;
	  }

	  }
	  
	  
	   if(cat=='deletequestion'){
	  
	  if(confirm("Do you want to delete this question")){
	  
	  var cat = this.id;
	  }else{
	  
	  return false;
	  }

	  }
	  
	  if(cat=="attempt_details"){
	 var user_id = $("#filter_user").val();
	 var quiz_id = $("#filter_quiz").val();
	 var url=cat+'=1&user_id='+user_id+'&quiz_id='+quiz_id;	  
	  }else{
	   var url=cat+'=1';
	  }		  
	//alert(course_id);
	
	  $("#loader").show();
          $("#disptitle").hide();	
          $.ajax({
            // Either GET or POST will work.
            type: 'POST',
            data: url,
            // Need to specify JSON data.
            dataType: 'json',
            url: $(this).attr('href'),
            success: function(response){
		    $("#loader").hide();
		     $("#disptitle").show();
		   //  sectionhead
		  var tit = $("#sectionhead").html("<b>" + ucfirst(cat)+ "</b>");
		  
		    
		    $("#sectionhead").text(tit);

		    
		    
              // Call all callbacks.
              if (response.__callbacks) {
                $.each(response.__callbacks, function(i, callback) { 
                  // The first argument is a target element, the second
                  // the returned JSON data.
                  eval(callback)(target, response);
                });
                // If you don't want to return this module's own callback,
                // you could of course just call it directly here.
                 Drupal.jqquizExample.formCallback(target, response);
              }
            }
          });
          return false;
        });
    });
};

/**
 * Ajax load example callback. 
 */
Drupal.jqquizExample.formCallback = function (target, response) {
  target = $(target).hide().html(response.content).fadeIn();
  Drupal.attachBehaviors(target);
};

})(jQuery);


function ucfirst(str) {
    var firstLetter = str.substr(0, 1);
    return firstLetter.toUpperCase() + str.substr(1);
}

function toggle_option(val){

         if(val==1){

	  $("#choicedata1").show();
	  $("#choice-tracks-single").show();
	  $("#choice-tracks-multi").hide();
	  $("#choicedata2").hide();
	  }
	  if(val==0){

	  $("#choicedata2").show();
	  $("#choice-tracks-single").hide();
	  $("#choice-tracks-multi").show();
	  $("#choicedata1").hide();
	  }

return true;


}