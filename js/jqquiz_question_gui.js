// $Id: jqquiz_question_gui.js,v 1.0 2012/04/05 14:01:06  $

(function ($) {

Drupal.jqquiz_question_gui = Drupal.jqquiz_question_gui || {};
/**
* Force a minimum width of a country field so that inline state fields
* don't jump around on the page.
*/
Drupal.behaviors.jqquiz_question_gui = function(context) {


  $('#startquizquestion', context).bind('click',function(){
  
 // start_quiz();
	  $("#loader").show();
	   $("#disptitle").hide();	
	   var target = $("#qcontent");
	      //var pageid = $("input#pageid").val();
	   var quiz_id = $("input#quiz_id").val();
	   var pageid = $("input#pageid").val();
	   
	   var question_id =   $("input#question_id").val();
	   var qtype_id =  $("input#question_type").val();
	   var answer_val = $("input#chk_answer").val();
	   var user_points = $("input#user_points").val();
	  var question_type = $("#question_type").val();
	   var total_time = $("#quiz_time").val();
	   // var last_question = $("#last_question").val();
	   var current_time=$("#count_val").val();
	   //	alert(time_taken);
	   var time_taken = total_time-current_time;
	  
	   var url = cat+'=1&quiz_id='+quiz_id+'&startquizquestion=startquizquestion&pagid='+pageid+'&question_id='+question_id+'&answer_val='+answer_val+'&user_points='+user_points+'&question_type='+question_type+'&time_taken='+time_taken;
           var cat = "question";

	
	 
	  
	 $.ajax({
            // Either GET or POST will work.
            type: 'POST',
            data: url,
            // Need to specify JSON data.
            dataType: 'json',
            url: $(this).attr('href'),
            success: function(response){
		    $("#loader").hide();
		     $("#disptitle").show();
		   //  sectionhead
		   //   var tit = $("#sectionhead").html("<b>"+ ucfirst(cat)+"</b>");
		   // $("#sectionhead").text(tit);
              // Call all callbacks.
              if (response.__callbacks) {
                $.each(response.__callbacks, function(i, callback) { 
                  // The first argument is a target element, the second
                  // the returned JSON data.
                  eval(callback)(target, response);
                });
                // If you don't want to return this module's own callback,
                // you could of course just call it directly here.
                 Drupal.jqquiz_question_gui.formCallback(target, response);
              }
            }
          });
  });
  
  
  
  
    $('#savequiz', context).bind('click',function(){
  
 // start_quiz();
 // $("#feedarrow").hide();
	  $("#loader").show();
	   $("#disptitle").hide();	
	   var target = $("#qcontent");
	      //var pageid = $("input#pageid").val();
	   var quiz_id = $("input#quiz_id").val();
	   var pageid = $("input#pageid").val();
	   
	   var question_id =   $("input#question_id").val();
	   var qtype_id =  $("input#question_type").val();
	   var answer_val = $("input#chk_answer").val();
	   var user_points = $("input#user_points").val();
     	  var question_type = $("#question_type").val();
	    var last_question = $("input#last_question").value=1;
	    var total_time = $("#quiz_time").val();
	     var current_time=$("#count_val").val();
	    var time_taken = total_time-current_time;
	   
	   var url = cat+'=1&quiz_id='+quiz_id+'&savequiz=savequiz&pagid='+pageid+'&question_id='+question_id+'&answer_val='+answer_val+'&user_points='+user_points+'&question_type='+question_type+'&last_question='+last_question+'&time_taken='+time_taken;
           var cat = "savequiz";
	//alert(url);
	
	 $.ajax({
            // Either GET or POST will work.
            type: 'POST',
            data: url,
            // Need to specify JSON data.
            dataType: 'json',
            url: $(this).attr('href'),
            success: function(response){
		    $("#loader").hide();
		     $("#disptitle").show();
		   //  sectionhead
		   //   var tit = $("#sectionhead").html("<b>"+ ucfirst(cat)+"</b>");
		   // $("#sectionhead").text(tit);
              // Call all callbacks.
              if (response.__callbacks) {
                $.each(response.__callbacks, function(i, callback) { 
                  // The first argument is a target element, the second
                  // the returned JSON data.
                  eval(callback)(target, response);
                });
                // If you don't want to return this module's own callback,
                // you could of course just call it directly here.
                 Drupal.jqquiz_question_gui.formCallback(target, response);
		$("div#countdown_text2").hide();
              }
            }
          });
  });
  
  
$('a#lightclose', context).bind('click',function(){
  $("#light").hide();
  $("#fade").hide();
   $("span#savequiz").show();   
  $("#savequiz").trigger("click");
	  
  });

    $('#backquizquestion', context).bind('click',function(){
     $("#loader").show();
     $("#disptitle").hide();	
   var target = $("#qcontent");
    var question_id =   $("input#question_id").val();
   //var pageid = $("input#pageid").val();
    var quiz_id = $("input#quiz_id").val();
    var pageid = $("input#backpageid").val();
     var answer_val = $("input#chk_answer").val();
     var user_points = $("input#user_points").val();
     var question_type = $("#question_type").val();
   
       
  var url = cat+'=1&quiz_id='+quiz_id+'&startquizquestion=startquizquestion&pagid='+pageid+'&question_id='+question_id+'&answer_val='+answer_val+'&user_points='+user_points+'&question_type='+question_type;
  var cat = "question";
            $.ajax({
            // Either GET or POST will work.
            type: 'POST',
            data: url,
            // Need to specify JSON data.
            dataType: 'json',
            url: $(this).attr('href'),
            success: function(response){
		    $("#loader").hide();
		     $("#disptitle").show();
		   //  sectionhead
		   //   var tit = $("#sectionhead").html("<b>"+ ucfirst(cat)+"</b>");
		   // $("#sectionhead").text(tit);
              // Call all callbacks.
              if (response.__callbacks) {
                $.each(response.__callbacks, function(i, callback) { 
                  // The first argument is a target element, the second
                  // the returned JSON data.
                  eval(callback)(target, response);
                });
                // If you don't want to return this module's own callback,
                // you could of course just call it directly here.
                 Drupal.jqquiz_question_gui.formCallback(target, response);
              }
            }
          });
  
  
  
  
  });
  
};



/**
 * Ajax load example callback. 
 */
Drupal.jqquiz_question_gui.formCallback = function (target, response) {
  target = $(target).hide().html(response.content).fadeIn();
  Drupal.attachBehaviors(target);
};

})(jQuery);