// $Id: jquiz_gui.js,v 1.0 2012/04/1 14:01:06 $

(function ($) {

Drupal.jqquiz_gui = Drupal.jqquiz_gui || {};

/**
* Force a minimum width of a country field so that inline state fields
* don't jump around on the page.
*/
Drupal.behaviors.jqquiz_gui = function(context) {
  
  $('#jqquiz_start_page', context).bind('change',function(){
     $("#loader").show();
          $("#disptitle").hide();	
   var target = $("#qcontent");
   var question_id = $("#question_id").val();
   var answer_val = $("input#chk_answer").val();
   var user_points = $("input#user_points").val();
   var question_type = $("#question_type").val();
   var cat = "question";
            $.ajax({
            // Either GET or POST will work.
            type: 'POST',
            data: cat+'=1&quiz_id='+this.value+'&jqquiz_start_page=jqquiz_start_page&question_id='+question_id+'&answer_val='+answer_val+'&user_points='+user_points+'&question_type='+question_type,
            // Need to specify JSON data.
            dataType: 'json',
            url: $(this).attr('href'),
            success: function(response){
		    $("#loader").hide();
		     $("#disptitle").show();
		   //  sectionhead
		   //   var tit = $("#sectionhead").html("<b>"+ ucfirst(cat)+"</b>");
		   // $("#sectionhead").text(tit);
              // Call all callbacks.
              if (response.__callbacks) {
                $.each(response.__callbacks, function(i, callback) { 
                  // The first argument is a target element, the second
                  // the returned JSON data.
                  eval(callback)(target, response);
                });
                // If you don't want to return this module's own callback,
                // you could of course just call it directly here.
                 Drupal.jqquiz_gui.formCallback(target, response);
              }
            }
          });
  
  
  });
  
  
};


/**
 * Ajax load example callback. 
 */
Drupal.jqquiz_gui.formCallback = function (target, response) {
  target = $(target).hide().html(response.content).fadeIn();
  Drupal.attachBehaviors(target);
};

})(jQuery);

function start_quiz(){
    $("#quiz_main").hide();
    $("#quiz_carocel").show();
$(document).ready(function(){
 //jQuery("#formID").validationEngine();
 
  var currentPosition = 0;
  var slideWidth = 760;
  var slides = $('.slide');
  var numberOfSlides = slides.length;

  // Remove scrollbar in JS
  $('#slidesContainer').css('overflow', 'hidden');

  // Wrap all .slides with #slideInner div
  slides.wrapAll('<div id="slideInner"></div>')
    // Float left to display horizontally, readjust .slides width
	.css({
      'float' : 'left',
      'width' : slideWidth
    });

  // Set #slideInner width equal to total width of all slides
  $('#slideInner').css('width', slideWidth * numberOfSlides);

  // Insert controls in the DOM
 // $('#slideshow')
  //  .prepend('<span class="control" id="leftControl">Clicking moves left</span>')
   // .append('<span class="control" id="rightControl">Clicking moves right</span>');

  // Hide left arrow control on first load
 // manageControls(currentPosition);

  // Create event listeners for .controls clicks
  $('.control')
    .bind('click', function(){

     currentPosition = ($(this).attr('id')=='rightControl') ? currentPosition+1 : currentPosition-1;
     manageControls(currentPosition);
    
     if($(this).attr('id')=='rightControl'){

//alert("asdad");
	jQuery("#formID").validationEngine({
        onValidationComplete: function(form, status){
	  
	 if (status==true) {
	    // Move slideInner using margin-left
		   $('#slideInner').animate({
		     'marginLeft' : slideWidth*(-currentPosition)
		   });

        $("span#feedback1").show();

        }else{
	     currentPosition = ($(this).attr('id')=='rightControl') ? currentPosition+1 : currentPosition-1;
	     manageControls(currentPosition);

	}
	
	}
	});
  
  
 }
  });

  // manageControls: Hides and Shows controls depending on currentPosition
  function manageControls(position){
    // Hide left arrow if position is first slide
	if(position==0){ $('#leftControl').hide() } else{ $('#leftControl').show() }
	// Hide right arrow if position is last slide
    if(position==numberOfSlides-1){ $('#rightControl').hide() } else{ $('#rightControl').show() }
  }
  
  

$("#rightControl").bind("click", function(){

    var correct_ans=$("#correct_answer").val();
    
    var sel = $("input:radio[name=group]:checked").val();
    
    var chk_len = $("input:[type=checkbox]").length;	
    
    var chk_cheked_len = $("input:checkbox[name=group]:checked").length;
     //var sel2 = $("input:checkbox[name=group]:checked").val();
    var answer_text = "";
    
    if(chk_len>1){
		var val = [];
		$("input:checkbox[name=group]:checked").each(function(i){
		val[i] = $(this).val();
		});
	
}
  if(val){
for(var j=0;j<val.length;j++){

answer_text+=val[j]+",";

}
    
  }  
    
    if(sel!=""){
    //$("#chk_answer").val();
      $("#chk_answer").val(sel);
    
    var flag;
    if(correct_ans==sel){
      $("#correct_feedback").show();
     $("#wrong_feedback").hide();
    flag=0;
      $("input#user_points").val(1);
    }
    if(correct_ans!=sel){
    $("#wrong_feedback").show();
    $("#correct_feedback").hide();
    flag=1;
      $("input#user_points").val(0);
    }
    }
    if(answer_text!=""){
    //$("#chk_answer").val();
    $("#chk_answer").val(answer_text);
    var flag;
    if(correct_ans==answer_text){
    
      $("#correct_feedback").show();
     $("#wrong_feedback").hide();
       $("input#user_points").val(1);
    flag=0;
    }
    if(correct_ans!=answer_text){
    
    
    $("#wrong_feedback").show();
    $("#correct_feedback").hide();
      $("input#user_points").val(0);
    flag=1;
    }
    }
   var feedbackpage = $("#feedbackpage").val();
    var fee_id = $("#pageid").val();
    var last_q = $("#numquest").val();
 //alert(feedbackpage);
    if(feedbackpage==last_q){
     if(flag=1 || flag==0) {
              $("span#submitquiz2").show();
              

         }
    } 
    correct_ans="";
    sel="";
    });  
});

}
  
